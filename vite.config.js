import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
import UnoCss from 'unocss/vite'
import { presetAttributify, presetUno } from 'unocss' // Presets

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  optimizeDeps: {
    include: ["axios"],
  },
  build: {
    target: "modules",
    outDir: "dist",
    assetsDir: "assets",
   // minify: "terser", // terser 将会强制降级为 'es2019'
  //   terserOptions: {
  //     compress: {
  //       drop_console: true, // 打包自动删除console
  //       drop_debugger: true
  //     },
  //     keep_classnames:true,
  //   },
   },
  server: {
    base: "/",
    host: "0.0.0.0",
    port: 8090,
    cors: true,  // 默认启用并允许任何源
    /*    proxy:{
          "/apis":{
            target:"http://192.168.2.172:57170",
            changeOrigin:true,
            rewrite:(path) => path.replace(/^\/apis/,"/"),
          },
        },*/
  },
  plugins: [vue(),UnoCss({
    presets: [presetAttributify(),presetUno()], // Presets
  })],
});
