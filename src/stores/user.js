import {defineStore} from "pinia";
import {b64_to_Json, b64_to_utf8} from "@/until/tool.js";

export const useInfoStore = defineStore({
  id: "userInfo",
  state: () => ({
    userInfoBase: String
  }),
  // 开启数据持久化
  persist: {
    enabled: true,
    strategies: [
      {
        key: "userInfo",
        storage: localStorage,
      },
    ],
  },
  getters: {},
  actions: {
    setUserInfoBase(data) {
      this.userInfoBase = data;
    },
  },
});


export function getUserInfoBase() {
  return  b64_to_Json(useInfoStore().userInfoBase)
}
