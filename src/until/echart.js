import * as echarts from "echarts";

export function setChart(main,options){
  setTimeout( () => {
    // 绘制图表
    const myChart = echarts.init( main );
    myChart.setOption( options );
    window.onresize = ()=>{
      myChart.resize()
    }
    window.addEventListener('resize', () => myChart.resize(), false)
  },300 );
}
