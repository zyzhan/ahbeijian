import {ref} from "vue";
import Http, {baseUrl} from "@/http/http.js";
import {useInfoStore} from "@/stores/user.js";
import {Message} from "@arco-design/web-vue";


export const pageObj = ref({
    total: 0,
    current: 1,
    pageSize: 8,
    showTotal: true,
    showJumper: false,
    showPageSize: true,
    pageSizeOptions: [15, 30, 50, 80, 100],
})

export const columns = ref([
    {title: "序号", key: "id", dataIndex: "id"},
    {title: "帐号", dataIndex: "UserAccount"},
    {title: "手机号", dataIndex: "UserPhone"},
    {title: "姓名", dataIndex: "UserName"},
    // {title: "身份", dataIndex: "permissionId"},
    {title: "注册时间", dataIndex: "CreateTime"}
])

export const pageClick = {
    tableClick: (current) => {
        pageObj.value.current = current;
    },
    pageSizeClick: (pageSize) => {
        pageObj.value.pageSize = pageSize;
        pageObj.value.current = 1;
    }
}


export const getImgUrl = (imgUrl) => {
    if (!imgUrl) return;
    if (imgUrl.length < 30) {
        return baseUrl + 'upload/' + imgUrl;
    } else {
        return imgUrl;
    }
}


// base64 加密
export function utf8_to_b64(str) {
    return btoa(unescape(encodeURIComponent(str)));
}

// base64 解密Json stringify
export function b64_to_Json(str) {
    if (typeof str === 'string') {
        return JSON.parse(window.decodeURIComponent(escape(atob(str))));
    } else {
        return {error: 0}
    }
}


// base64 解密用于与后端传输加密
export function b64_to_utf8(str) {
    return window.decodeURIComponent(escape(atob(str)));
}

export const fieldNames = {value: 'TypeId', label: 'TypeName'}

export function treeData(source, id, parentId, children) {
    let cloneData = JSON.parse(JSON.stringify(source));
    return cloneData.filter(father => {
        // 顶级过滤
        let branchArr = cloneData.filter(child => father[id] === child[parentId]);
        // 子级过滤
        branchArr.length > 0 ? father[children] = branchArr : "";
        return father[parentId] === 0;    // 如果第一层不是parentId=0，请自行修改
    });
}

export function filterTreeNode(searchValue, nodeData) {
    return nodeData.TypeName.toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
}


export const treeField = {
    key: "TypeId",
    title: "TypeName",
    children: "children",
}

export const subData = async (data) => {
    if (!data.GuestbookName) {
        Message.error("请填写您的称呼!")
        return
    }
    if (!data.GuestbookPhone) {
        Message.error("请填写您的电话!")
        return
    }
    const res = await Http.guestbookAdd(data)
    if (res.Data) {
        Message.success("留言成功!10分钟内，我们将安排工作人员和您联系!")
    }
}
