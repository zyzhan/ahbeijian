import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from "./stores/index";
import xeEutils from "xe-utils";
import { XeFun } from "./until/xeUt";
import Http from "./http/http";
import ArcoVue, { Message } from "@arco-design/web-vue";
import "./assets/layout.less";
import ArcoVueIcon from "@arco-design/web-vue/es/icon";
import "@arco-design/web-vue/dist/arco.css";
import { useInfoStore } from "./stores/user.js";
import mavonEditor from "mavon-editor";
import "mavon-editor/dist/css/index.css";
import "./assets/layout.less";
import * as echarts from "echarts";
import ECharts from "vue-echarts";
import 'uno.css'

const app = createApp(App);
app.component("VChart", ECharts);
xeEutils.mixin(XeFun);
app.config.globalProperties.$Http = Http;
app.config.globalProperties.$message = Message;
app.config.globalProperties.$XEUtils = xeEutils;
app.use(router).use(store).use(ArcoVue).use(ArcoVueIcon).use(mavonEditor).mount("#app");
app.config.globalProperties.$userStore = useInfoStore();
app.config.globalProperties.$Echarts = echarts;
