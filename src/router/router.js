import {createRouter, createWebHistory} from "vue-router";
import IndexLay from "../layout/layout.vue";
import webLayout from "../views/webLayout/webLayout.vue";

export const menuRouter = [
    {path: "/member/home", component: () => import("../vManage/home/Home.vue"), title: "主页", icon: "icon-home", auth: [1, 2, 3, 9]},
    {path: "/member/userEdit", component: () => import("../vManage/users/userEdit.vue"), title: "用户编辑", icon: "icon-home", auth: []},
    {path: "/member/cert", component: () => import("../vManage/cert.vue"), title: "证书管理", icon: "icon-home", auth: [3, 9]},
    {
        path: "/member/news", title: "资讯管理", icon: "icon-user", redirect: "/member/newsList", isUp: true, auth: [3, 9],
        children: [
            {path: "/member/newsList", component: () => import("../vManage/news/newsList.vue"), title: "资讯列表", icon: "icon-list", auth: [3, 9]},
            {path: "/member/newsType", component: () => import("../vManage/news/newsType.vue"), title: "资讯分类", icon: "icon-list", auth: [3, 9]},
            {path: "/member/newsEdit", component: () => import("../vManage/news/newsEdit.vue"), title: "编辑资讯", icon: "icon-home", isShow: true},
        ]
    },

     {
         path: "/profession", title: "行业检测", icon: "icon-user", redirect: "/member/professionList", isUp: true, auth: [3, 9],
         children: [
             {path: "/profession/professionList", component: () => import("../vManage/profession/professionList.vue"), title: "行业检测列表", icon: "icon-list", auth: [3, 9]},
             {path: "/profession/professionType", component: () => import("../vManage/profession/professionType.vue"), title: "行业检测分类", icon: "icon-list", auth: [3, 9]},
             {path: "/profession/professionDetail", component: () => import("../vManage/profession/professionDetail.vue"), title: "行业检测内容", icon: "icon-home", isShow: true},
         ]
     },
     {
         path: "/serviceManage", title: "服务项目管理", icon: "icon-user", redirect: "/serviceManage/serviceList", isUp: true, auth: [3, 9],
         children: [
             {path: "/serviceManage/serviceList", component: () => import("../vManage/service/serviceList.vue"), title: "服务项目列表", icon: "icon-list", auth: [3, 9]},
             {path: "/serviceManage/serviceType", component: () => import("../vManage/service/serviceType.vue"), title: "服务项目分类", icon: "icon-list", auth: [3, 9]},
             {path: "/serviceManage/serviceDetail", component: () => import("../vManage/service/serviceDetail.vue"), title: "服务项目内容", icon: "icon-home", isShow: true},
         ]
     },
    {path: "/member/page", component: () => import("../vManage/page/page.vue"), title: "单页管理", icon: "icon-list", auth: [3, 9]},
    {path: "/member/guestbook", component: () => import("../vManage/guestbook/guestbook.vue"),title: "留言管理", icon: "icon-list",auth: [3, 9]},
    {path: "/member/images", component: () => import("../vManage/imgMange.vue"), title: "图片管理", icon: "icon-list", auth: [3, 9]},
    {path: "/member/userList", component: () => import("../vManage/users/userList.vue"), title: "用户管理", icon: "icon-list", auth: [3, 9]},
    {path: "/member/pageDetail", component: () => import("../vManage/page/pageDetail.vue"), title: "单页编辑", icon: "icon-list", auth: []},
];
const webRouter = [
    {path: "/", component: () => import("../views/home/home.vue")},
    {path: "/check", component: () => import("../views/check/check.vue")},
    {path: "/checkDetail/:id", component: () => import("../views/check/checkDetail.vue")},
    {path: "/service", component: () => import("../views/service/service.vue")},
    {path: "/serviceDetail/:id", component: () => import("../views/service/serviceDetail.vue")},
    {path: "/news", component: () => import("../views/news/news.vue")},
    {path: "/newsDetail/:id", component: () => import("../views/news/newsDetail.vue")},
    {path: "/about", component: () => import("../views/about/about.vue")},
    {path: "/company", component: () => import("../views/about/company.vue")},
    {path: "/contact", component: () => import("../views/about/contact.vue")},
    {path: "/guestbook", component: () => import("../views/guestbook/guestbook.vue")},
    {path: "/cert", component: () => import("../views/cert.vue")},
];
const routes = [
    {path: "/hello", component: () => import("../components/HelloWorld.vue")},
    {path: "/login", component: () => import("../vManage/Login.vue")},
    {path: "/register", component: () => import("../views/register.vue")},
    {
        path: "/member", component: IndexLay, redirect: "/member/home",
        children: menuRouter,
    },
    {
        path: "/service", component: webLayout, redirect: "/service",
        children: webRouter,
    },
    // 404
    {path: "/:path(.*)", redirect: "/log    in"},
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
